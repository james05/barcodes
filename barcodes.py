#!/usr/bin/python

# http://effbot.org/imagingbook/image.htm#tag-Image.new

# Usage: barcodes.py FILE_NAME NUM_PAGES

import Image
import sys
from code128 import *
import os

file_name = sys.argv[1] 

f = open(file_name, "r")

NUM_PAGES = int(sys.argv[2])
NUM_PER_PAGE = 30
START_BAR_CODE = int(f.read()) 

f.close()

VERTICAL_CELL_MARGIN = 30
TOP_MARGIN = 70 # 0.5"
BOTTOM_MARGIN = 10 # 0.1"
LEFT_MARGIN = 20 # 0.1"
RIGHT_MARGIN = 10 # 0.1"
CELL_WIDTH = 2*266 # 2.77"
CELL_HEIGHT =  96 # 1"
LABELS_PER_COLUMN = 10
LABELS_PER_ROW = 3

WIDTH = LABELS_PER_COLUMN * CELL_HEIGHT + TOP_MARGIN
HEIGHT = LABELS_PER_ROW * CELL_WIDTH + LEFT_MARGIN + RIGHT_MARGIN

bar = Code128()

bar_code = START_BAR_CODE

for page in range(0, NUM_PAGES):

	img = Image.new('RGB', (WIDTH,HEIGHT), "white")

	rows = TOP_MARGIN
	cols = 1
	count = 0
	col = 0

	for i in range(0, NUM_PER_PAGE):
		# height 50, png
	   bar.getImage(str(bar_code), 50, "png") # 150 by 50
	   im = Image.open("barcodes/" + str(bar_code) + '.png')
	   # im.thumbnail((150, 50))

	   count = count + 1

	   # column first (after 3 reset to 0)
	   # row (every image increases rows by 50 + vertical margin
	   img.paste(im, (col, rows))
	   os.unlink("barcodes/" + str(bar_code) + ".png")

	   bar_code = bar_code + 1
	   col = col + (CELL_WIDTH - im.size[0])

	   if count == 3:
	     rows = rows + (CELL_HEIGHT - im.size[1] + 90) + VERTICAL_CELL_MARGIN
	     print(str(rows))
	     col = 0
	     count = 0


	img.save("barcodes/page" + str(page) + ".png", "png")

# print next bar_code to use on next run
f = open(file_name, "w")

f.write(str(bar_code))

f.close()